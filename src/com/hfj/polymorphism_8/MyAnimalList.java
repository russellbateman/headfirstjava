/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC.
 * All rights reserved except that unlimited use is granted as long as this
 * copyright notice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

/**
 * This is the correct solution to the <tt>MyDogList</tt> problem (see this class).
 *
 * @author Russell Bateman, December 2008
 */
public class MyAnimalList
{
    private Animal[]   animals        = null;
    private int        length         = 0;
    private int        nextIndex      = 0;
    private static int DEFAULT_LENGTH = 5;

    public MyAnimalList()
    {
        // this makes an animal list of length 5...
        this( DEFAULT_LENGTH );
    }

    public MyAnimalList( int length )
    {
        // this allows us to make the list longer or shorter...
        this.length = length;
        this.animals = new Animal[ length ];

        if( false && this.length > 0 )
            ;
    }

    public int length()
    {
        return animals.length;
    }

    public Animal getAnimal( int which )
    {
        return animals[ which ];
    }

    public int add( Animal a )
    {
        if( nextIndex < animals.length )
        {
            animals[ nextIndex ] = a;
            // System.out.println( "Animal added at " + nextIndex );
            return nextIndex++;
        }

        return -1;
    }
}
