/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

public class Hippopotamus extends Animal
{
    public Hippopotamus() { this.kind = "hippopotamus"; }

    public void roam()
    {
        System.out.println( "The hippopotamus pads." );
    }

    public void eat()
    {
        System.out.println( "The hippopotamus munches." );
    }

    public void makeNoise()
    {
        System.out.println( "The hippopotamus roars." );
    }

    public void sleep()
    {
        System.out.println( "The hippopotamus burbles." );
    }
}
