/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

/**
 * <h3> Head First Java, Chapter 8, Polymorphism </h3>
 *
 * <p>
 * Here is my implementation of <tt>Animal</tt> to which I've added a name and a
 * kind for nice console print statements.
 * </p>
 *
 * <p>
 * See <tt>Dog</tt> for the best implementation comments on the <tt>Pet</tt>
 * implementation that augments <tt>Animal</tt>s that can be pets.
 * </p>
 *
 * @author Russell Bateman, December 2008
 */
public abstract class Animal
{
    int    picture, food, hunger, boundaries, location;
    String kind;
    String name;    // used only by Animals that implement Pet

    abstract void makeNoise();
    abstract void roam();
    abstract void eat();
    abstract void sleep();

    // there will probably never be a reason to override these...
    public String getKind() { return this.kind; }
    public String getName() { return this.name; }
}
