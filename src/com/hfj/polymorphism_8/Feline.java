/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

public abstract class Feline extends Animal
{
    abstract void roam();
}
