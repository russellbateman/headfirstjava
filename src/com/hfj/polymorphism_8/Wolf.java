/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

public class Wolf extends Canine
{
    public Wolf() { this.kind = "wolf"; }

    public void roam()
    {
        System.out.println( "The wolf roams." );
    }

    public void makeNoise()
    {
        System.out.println( "The wolf snarls." );
    }

    public void eat()
    {
        System.out.println( "The wolf gulps down." );
    }

    public void sleep()
    {
        System.out.println( "The wolf sleeps." );
    }

    public static void main( String[] args )
    {
        Wolf wolf = new Wolf();

        wolf.roam();
        wolf.makeNoise();
        wolf.eat();
        wolf.sleep();
    }
}
