/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

public class Cat extends Feline implements Pet
{
    public Cat()
    {
        this( null );
    }

    public Cat( String name )
    {
        this.kind = "cat";

        if( name != null )
            this.name = name;
    }

    public boolean isAPet()
    {
        return( this.name != null );
    }

    public void roam()
    {
        System.out.println( "The cat creeps." );
    }

    public void eat()
    {
        System.out.println( "The cat devours." );
    }

    public void makeNoise()
    {
        System.out.println( "The cat meows." );
    }

    public void sleep()
    {
        System.out.println( "The cat purrs." );
    }

    public void beFriendly()
    {
        System.out.println( this.name + " is being friendly." );
    }

    public void play()
    {
        System.out.println( this.name + " is playing." );
    }
}
