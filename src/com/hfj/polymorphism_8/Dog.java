/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

/**
 * A <tt>Dog</tt> is not only an animal, but a <tt>Pet</tt> as well. Since Java
 * prohibits (fails to support) multiple inheritance, we just implement the
 * interface <tt>Pet</tt>&mdash;giving us <tt>Pet</tt> characteristics in addition
 * to those shared by <tt>Animal</tt>s.
 * </p>
 *
 * <p>
 * We distinguish pet dogs from strays basically on the basis of whether they
 * have a name or not.
 * </p>
 *
 * @author Russell Bateman
 */
public class Dog extends Canine implements Pet
{
    public Dog()
    {
        // constructor that gives the Dog no name: it's a stray...
        this( null );
    }

    public Dog( String name )
    {
        // constructor allowing us to name the Dog which makes it a Pet...
        this.kind = "dog";

        if( name != null )
            this.name = name;
    }

    public boolean isAPet()
    {
        return( this.name != null );
    }

    public void roam()
    {
        if( name != null )
            System.out.println( this.name + " roams." );
        else
            System.out.println( "The dog roams." );
    }

    public void makeNoise()
    {
        if( name != null )
            System.out.println( this.name + " says, \"Woof!\"." );
        else
            System.out.println( "The dog says, \"Woof!\"." );
    }

    public void eat()
    {
        if( name != null )
            System.out.println( this.name + " eats." );
        else
            System.out.println( "The dog eats." );
    }

    public void sleep()
    {
        if( name != null )
            System.out.println( this.name + " sleeps." );
        else
            System.out.println( "The dog sleeps." );
    }

    public void beFriendly()
    {
        // only Pets can do this...
        System.out.println( this.name + " is being friendly." );
    }

    public void play()
    {
        // only Pets can do this...
        System.out.println( this.name + " is playing." );
    }
}
