/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

public class Lion extends Feline
{
    public Lion() { this.kind = "lion"; }

    public void roam()
    {
        System.out.println( "The lion pads." );
    }

    public void eat()
    {
        System.out.println( "The lion devours." );
    }

    public void makeNoise()
    {
        System.out.println( "The lion roars." );
    }

    public void sleep()
    {
        System.out.println( "The lion snores." );
    }
}
