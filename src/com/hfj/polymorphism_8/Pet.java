/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

/**
 * We tried to augment <tt>Pet</tt> with a name field since <tt>Animal</tt>s would
 * not really have names (only "kinds"), but an interface cannot usefully contain
 * data, so we had to leave the name field in the <tt>Animal</tt> class. See this
 * class and also <tt>Dog</tt>, our fully commented <tt>Animal</tt>-and-<tt>Pet</tt> class.
 *
 * @author Russell Bateman
 */
public interface Pet
{
    public abstract boolean isAPet();
    public abstract void    beFriendly();
    public abstract void    play();
}
