/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

/**
 * This silly experiment just demonstrates "renaming" a class whose name we don't
 * like (or for some other reason).
 *
 * @author Russell Bateman
 */
public class Hippo extends Hippopotamus
{
}
