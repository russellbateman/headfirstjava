/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

/**
 * This demonstrates avoiding the true benefits of polymorphism by creating an
 * array of <tt>Dogs</tt> that will not work for <tt>Cats</tt> or <tt>Felines</tt> or
 * anything else. So, we'd have to make a <tt>MyCatList</tt>, a <tt>MyLionList</tt>,
 * a <tt>MyWolfList</tt>, etc. Instead, we'll create a <tt>MyAnimalList</tt> (see
 * this class).
 *
 * @author Russell Bateman, December 2008
 */
public class MyDogList
{
    private Dog[] dogs      = new Dog[ 5 ];
    private int   nextIndex = 0;

    public void add( Dog d )
    {
        if( nextIndex < dogs.length )
        {
            dogs[ nextIndex ] = d;

            if( d.name != null )
                System.out.println( "Dog " + d.name + " added at " + nextIndex );
            else
                System.out.println( "Dog added at " + nextIndex );
            nextIndex++;
        }
    }

    public Dog getDog( int which )
    {
        return ( dogs[ which ] != null ) ? dogs[ which ] : null;
    }

    // test code...
    static String[] names = { "Pooch", "Fido", "Nelson", "Juana", null };

    public static void main( String[] args )
    {
        MyDogList list = new MyDogList();

        for( int i = 0; i < 5 ; i++ )
        {
            Dog dog = ( names[ i ] != null ) ? new Dog( names[ i ] ) : new Dog();
            list.add( dog );
        }

        int    i = 0;

        while( i < 5 )
        {
            Dog d = list.getDog( i );

            if( d != null )
                d.makeNoise();
            else
                System.out.println( "Out of dogs, Max!" );

            i++;
        }
    }
}
