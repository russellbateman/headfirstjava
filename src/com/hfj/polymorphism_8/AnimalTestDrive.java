/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.polymorphism_8;

/**
 * <h3> Head First Java, Chapter 8, Polymorphism </h3>
 *
 * <p>
 * This tests <tt>MyAnimalList</tt> and the other <tt>Animal</tt> classes. You'll
 * want to pay attention to the little tricks employed here like the use of
 * <tt>Pet</tt>..
 * </p>
 *
 * @author Russell Bateman, December 2008
 */
public class AnimalTestDrive
{
    static String kind( Animal a )
    {
        if( a instanceof Cat )
            return "cat";
        else if( a instanceof Dog )
            return "dog";
        else if( a instanceof Lion )
            return "lion";
        else if( a instanceof Wolf )
            return "wolf";
        else
            return null;
    }

    static String indef_art( String word )
    {
        char    first = word.charAt( 0 );

        switch( first )
        {
            default :   return "a";
            case 'u' :  return "an";
            case 'a' : case 'e' : case 'i' : case 'o' : return "an";
        }
    }

    public static void main( String[] args )
    {
        int          instance = -1;
        MyAnimalList list = new MyAnimalList( 6 );

        Dog   d = new Dog( "Max" );
        Cat   c = new Cat( "Jacqueline" );
        Wolf  w = new Wolf();
        Lion  l = new Lion();
        Dog   D = new Dog();
        Hippo h = new Hippo();

        System.out.print( "Animals added at " );
        instance = list.add( d ); System.out.print(      + instance );
        instance = list.add( c ); System.out.print( ", " + instance );
        instance = list.add( w ); System.out.print( ", " + instance );
        instance = list.add( l ); System.out.print( ", " + instance );
        instance = list.add( D ); System.out.print( ", " + instance );
        instance = list.add( h ); System.out.print( ", " + instance );
        System.out.println();

        for( int i = 0; i < list.length(); i++ )
        {
            Animal    a    = list.getAnimal( i );

            if( a == null )
            {
                System.out.println( "There is no animal number " + i + "." );
                continue;
            }

            /* We've equipped Animal with a "kind" instance that is convenient for
             * use in printing to the console.
             */
            String    kind = kind( a );
            if( kind == null )
                kind = "unknown";

            /* To ensure that this animal is a pet, we check using "instanceof."
             */
            if( a != null && a instanceof Pet )
            {
                String name = a.getName();
                Pet    p    = ( Pet ) a;

                if( name != null )
                {
                    kind = indef_art( kind ) + " " + kind;
                    System.out.println( name + ", " + kind + ", says hello" );
                    p.beFriendly();
                }
                else
                {
                    System.out.println( "This " + kind + " has no name; would you like to adopt it?" );
                }
            }
            else
            {
                System.out.println( "The " + a.getKind() + " is wild and does not say hello." );
            }
        }
    }
}
