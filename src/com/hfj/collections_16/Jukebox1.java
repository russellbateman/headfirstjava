/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.collections_16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Tracking song popularity on your Jukebox... This illustrates that
 * <tt>ArrayList</tt> doesn't have a sorting method. However, we can use the
 * <tt>Collections.sort()</tt> method.
 * </p>
 *
 * <p>
 * This chapter is mostly to learn "generics" in Java. Virtually all of the code
 * that one writes dealing with generics is collection-related (think of the
 * &lt; ... &gt; brackets).
 * </p>
 *
 * <p>
 * See <tt>Jukebox3</tt> for details.
 * </p>
 *
 * @author Russell Bateman, December 2008
 */
public class Jukebox1
{
    static String			RESOURCES = "src/resources";
    ArrayList< String >	songList  = new ArrayList< String >();

    private void addSong( String lineToParse )
    {
        String[]	tokens = lineToParse.split( "/" );

        // keep only the song title...
        songList.add( tokens [ 0 ] );
    }

    private void getSongs()
    {
        try
        {
            File						f      = new File( RESOURCES + "/SongList.txt" );
            BufferedReader	reader = new BufferedReader( new FileReader( f ) );
            String					line   = null;

            while( ( line = reader.readLine() ) != null )
                addSong( line );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }

    /**
     * Starts loading the song file and then prints the contents of the song list
     * in unsorted, then sorted fashion demonstrating the utility of Java's
     * <tt>Collections</tt> class..
     */
    public void go()
    {
        getSongs();

        // ArrayList doesn't have a sorting method...
        System.out.println( "Not sorted:" );
        System.out.println( songList );
        System.out.println();

        // However, this sorts the array...
        Collections.sort( songList );

        // ...and we can try it again...sorted!
        System.out.println( "Sorted by song:" );
        System.out.println( "  " + songList );
        System.out.println();
    }

    public static void main( String[] args )
    {
        new Jukebox1().go();
    }
}
