package com.hfj.collections_16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Tracking song popularity on your Jukebox... HFJ called this version 5 even
 * though there was no version 4. Anyway, this continues to be all about using
 * generics, but in this case, specifically about <tt>Comparator</tt>.
 * </p>
 *
 * @author Russell Bateman, December 2008
 */
public class Jukebox6
{
    ArrayList< Song > songList  = new ArrayList< Song >();

    private void addSong( String lineToParse )
    {
        String[] tokens   = lineToParse.split( "/" );
        Song     nextSong = new Song( tokens[ 0 ], tokens[ 1 ], tokens[ 2 ], tokens[ 3 ] );

        // keep only the song title...
        songList.add( nextSong );
    }

    private void getSongs()
    {
        try
        {
            File           f      = new File( Jukebox1.RESOURCES + "/SongListMore.txt" );
            BufferedReader reader = new BufferedReader( new FileReader( f ) );
            String         line   = null;

            while( ( line = reader.readLine() ) != null )
                addSong( line );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }

    /**
     * Starts loading the song file and then prints the contents of the song list
     * in unsorted, then sorted fashion demonstrating the utility of Java's
     * <i>Collections</i> class..
     */
    public void go()
    {
        getSongs();

        // ArrayList doesn't have a sorting method...
        System.out.println( "Not sorted:" );
        System.out.println( "  " + songList );
        System.out.println();

        // However, this sorts the array...
        Collections.sort( songList );

        // ...and we can try it again...sorted!
        System.out.println( "Sorted by song:" );
        System.out.println( "  " + songList );
        System.out.println();

        ArtistCompare aC = new ArtistCompare();
        Collections.sort( songList, aC );

        // ...and now, sorted by artist!
        System.out.println( "Songs sorted by artist:" );
        System.out.println( "  " + songList );
        System.out.println();

        HashSet< Song > songSet = new HashSet< Song >();

        songSet.addAll( songList );

        System.out.println( "Presorted using HashSet:" );
        System.out.println( "  " + songSet );
        System.out.println();
    }

    public static void main( String[] args )
    {
        new Jukebox6().go();
    }
}
