/* ============================================================================
 * Copyright (c) 2008 by Russell Bateman and Etretat Logiciels, LLC. All rights
 * reserved except that unlimited use is granted as long as this copyright not-
 * ice appears in the source.
 * ============================================================================
 * Head First Java implied chapter exercise.
 * ============================================================================
 */
package com.hfj.collections_16;

import java.util.Comparator;

/**
 * This implementation of <tt>Comparator</tt> is a helper class to <tt>Jukebox5</tt>
 * for sorting titles by artist.
 *
 * @author Russell Bateman
 */
public class ArtistCompare implements Comparator< Song >
{
    public int compare( Song one, Song two )
    {
        return one.getArtist().compareTo( two.getArtist() );
    }
}
