package com.hfj.collections_16;

public class Song implements Comparable< Song >
{
    String    title, artist, rating, bpm;

    public Song( String title, String artist, String rating, String bpm )
    {
        this.title  = title;
        this.artist = artist;
        this.rating = rating;
        this.bpm    = bpm;
    }

    public String getTitle() { return title; }
    public void setTitle ( String title ) { this.title = title; }

    public String getArtist() { return artist; }
    public void setArtist( String artist ) { this.artist = artist; }

    public String getRating() { return rating; }
    public void setRating( String rating ) { this.rating = rating; }

    public String getBpm() { return bpm; }
    public void setBpm   ( String bpm ) { this.bpm = bpm; }


    public String toString()
    {
        /* We override this method because when System.out.println() gets it, it will
         * call toString() on each element in the list and we only want titles.
         */
        return this.title;
    }

    public boolean equals( Object o )
    {
        Song s = ( Song ) o;
        return getTitle().equals(  s.getTitle() );
    }

    public int hashCode()
    {
        return this.title.hashCode();
    }

    public int compareTo( Song s )
    {
        return this.title.compareTo( s.getTitle() );
    }
}
