package com.hfj.collections_16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Tracking song popularity on your Jukebox... HFJ called this version 3 even
 * though there was no version 2. Anyway, this is all about using generics.
 * </p>
 *
 * <p>
 * <tt>ArrayList</tt> is the most-used generified type.
 * </p>
 *
 * <h2> Learning generics </h2>
 *
 * <p>
 * <b>Creating instances of generified classes.</b> When you make an
 * <tt>ArrayList</tt>, you have to tell it the type of objects you'll allow in the
 * list&mdash;just as with plain, old arrays.
 * </p>
 *
 * <pre>
 *  new ArrayList&lt;&nbsp;Song&nbsp;&gt;() </code>
 * </pre>
 *
 * <p>
 * <b>Declaring and assigning variables of generic types.</b> If you have an
 * <tt>ArrayList&lt; Animal &gt;</tt> reference variable, you can assign an
 * <tt>ArrayList&lt; Dog &gt;</tt> to it.
 * </p>
 *
 * <pre>
 *  List&lt; Song &gt; songList = new ArrayList&lt;&nbsp;Song&nbsp;&gt;();
 * </pre>
 *
 * <p>
 * <b>Declaring (and invoking) methods that take generic types.</b> If you have
 * a method that takes as a parameter, e.g.: an <tt>ArrayList</tt> of <tt>Animal</tt>
 * objects, you can also pass it an <tt>ArrayList</tt> of <tt>Dog</tt> objects.
 * </p>
 *
 * <pre>
 *  void foo( List&lt; Song &gt; list )
 *  {
 *    ...
 *  }
 *
 *  x.foo( songList );
 * </pre>
 *
 * @author Russell Bateman, December 2008
 */
public class Jukebox3
{
    ArrayList< Song > songList  = new ArrayList< Song >();

    private void addSong( String lineToParse )
    {
        String[]    tokens   = lineToParse.split( "/" );
        Song            nextSong = new Song( tokens[ 0 ], tokens[ 1 ], tokens[ 2 ], tokens[ 3 ] );

        // keep only the song title...
        songList.add( nextSong );
    }

    private void getSongs()
    {
        try
        {
            File           f      = new File( Jukebox1.RESOURCES + "/SongListMore.txt" );
            BufferedReader reader = new BufferedReader( new FileReader( f ) );
            String         line   = null;

            while( ( line = reader.readLine() ) != null )
                addSong( line );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }

    /**
     * Starts loading the song file and then prints the contents of the song list
     * in unsorted, then sorted fashion demonstrating the utility of Java's
     * <tt>Collections</tt> class..
     */
    public void go()
    {
        getSongs();

        // ArrayList doesn't have a sorting method...
        System.out.println( "Not sorted:" );
        System.out.println( "  " + songList );
        System.out.println();

        // However, this sorts the array...
        Collections.sort( songList );

        // ...and we can try it again...sorted!
        System.out.println( "Sorted by song:" );
        System.out.println( "  " + songList );
        System.out.println();
    }

    public static void main( String[] args )
    {
        new Jukebox3().go();
    }
}
